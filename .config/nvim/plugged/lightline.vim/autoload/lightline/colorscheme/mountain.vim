" =============================================================================
" Filename: autoload/lightline/colorscheme/molokai.vim
" Author: challsted
" License: MIT License
" Last Change: 2020/02/15 20:57:45.
" =============================================================================

let s:black = [ '#0f0f0f', 233 ]
let s:b_black = [ '#191919', 244 ]
let s:white = [ '#f0f0f0', 234 ]
let s:blue = [ '#8f8aac', 81 ]
let s:b_red = [ '#ac8aac', 161 ]
let s:red = [ '#ac8a8c', 160 ]
let s:yellow = [ '#aca98a', 229 ]

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left = [ [ s:b_black, s:red ], [ s:white, s:b_black] ]
let s:p.normal.middle = [ [ s:white, s:black ] ]
let s:p.normal.right = [ [ s:b_red, s:b_black ], [ s:b_black, s:b_red ] ]
let s:p.normal.error = [ [ s:b_red, s:black ] ]
let s:p.normal.warning = [ [ s:yellow, s:black ] ]
let s:p.insert.left = [ [ s:black, s:blue ], [ s:white, s:black ] ]
let s:p.visual.left = [ [ s:black, s:yellow ], [ s:white, s:black ] ]
let s:p.replace.left = [ [ s:black, s:red ], [ s:red, s:black ] ]
let s:p.inactive.left =  [ [ s:b_red, s:black ], [ s:white, s:black ] ]
let s:p.inactive.middle = [ [ s:b_black, s:black ] ]
let s:p.inactive.right = [ [ s:white, s:b_red ], [ s:b_red, s:black ] ]
let s:p.tabline.left = [ [ s:b_red, s:black ] ]
let s:p.tabline.middle = [ [ s:b_red, s:black] ]
let s:p.tabline.right = copy(s:p.normal.right)
let s:p.tabline.tabsel = [ [ s:black, s:b_red ] ]

let g:lightline#colorscheme#mountain#palette = lightline#colorscheme#flatten(s:p)
