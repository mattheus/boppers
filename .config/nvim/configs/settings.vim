" Indentation
set autoindent                          " Good auto indent
set smartindent                         " Makes indenting smart
set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
set softtabstop=4                       " Tabstop but with more stuff
set showmatch

" Search
set hlsearch                            " Highlight matches
set ignorecase                          " Ignore case when searching
set incsearch                           " Incremental search
set smartcase                           " Ignore case when only lower case is typed

" Theming
set background=dark                     " Controls the theme's mode, light, dark, if possible.
set colorcolumn=80                      " Adds a column at the 80th character
set cursorline                          " Line at the cursor
set guioptions-=e                       " Don't use GUI tabline
set termguicolors                       " Makes themes less gross
colorscheme mountain                   " Chad mountain theme
set t_Co=256                            " Adds support 256 colors
syntax enable                           " Syntax highlighting

" Misc
set nocompatible
filetype plugin on
set number relativenumber               " Cool numbers
set cmdheight=2                         " More space for displaying messages
set clipboard=unnamedplus               " Global copy and paste"
set formatoptions-=cro                  " Stop new lines continuing a former lines comments.
set grepprg=rg\ --vimgrep               " Set default grepper
set mouse=a                             " Enables mouse support and prevents mouse from copying line numbers. Thank you, Stack Overflow.
set noshowmode                          " Removes the insert prompt
" set nowrap                              " Display long lines and one rather then multiple
set pumheight=10                        " Prompt height"
set scrolloff=3                         " Keeps the cursor away from the screen a set ammount when scrolling
set showtabline=2                       " Always show tabs
set showtabline=2                       " Show tabline
set splitbelow                          " Horizontal splits are always on the bottom
set splitright                          " Vertical splits are always on the right

" No trailing white space after exit
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e

" Clears LaTeX logs after exit
autocmd VimLeave *.tex !texclear %

" Goyo
autocmd FileType markdown Goyo
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" File types
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff

" Change completion bindings, thank you Nemo!
noremap <Tab> <>
noremap <S-Tab> <Up>

" Change completion bindings, thank you Nemo!
noremap <Tab> <>
noremap <S-Tab> <Up>

" Change completion bindings, thank you Nemo!
noremap <Tab> <>
noremap <S-Tab> <Up>

" Ntrw settings
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 20

" Open Netrw on start up
" autocmd VimEnter * Lexplore
