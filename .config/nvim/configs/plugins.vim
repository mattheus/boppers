call plug#begin()

" Functionality
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'mbbill/undotree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'vimwiki/vimwiki'
Plug 'voldikss/vim-floaterm'
Plug 'ap/vim-css-color'

" Aesthetics
Plug 'pradyungn/Mountain', { 'rtp': 'vim' }
Plug 'tpope/vim-markdown'
Plug 'cespare/vim-toml'

call plug#end()
