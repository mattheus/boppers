" So wraping isn't stupid
nnoremap <expr> j (v:count > 4 ? "m'" . v:count . 'j' : 'gj')
nnoremap <expr> k (v:count > 4 ? "m'" . v:count . 'k' : 'gk')

" Leader stuff
let mapleader=" "
nnoremap <Space> <Nop>
set showcmd

" Clear search results
nnoremap <silent> // :noh<CR>

" Window commands
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Tab navigation
nnoremap t :tabnew<CR>
nnoremap <C-u> :tabn<CR>
nnoremap <C-i> :tabp<CR>

" Sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" LaTeX, baby!
nnoremap <leader>c :! compiler %<CR>
nnoremap <leader>p :! opout %<CR>

" Goyo
nnoremap <leader>G :Goyo<CR>

" Toggle Spell check
nnoremap <leader>s :set spell!<CR>

" Toggle netrw
nnoremap <leader>n :Lexplore<CR><CR>
